# EEE3088 PIHAT PROJECT


Our group is designing a low cost UPS hat. The purpose of the hat is to allow the user to have a source of portable power for on-the-go situations and for when there are power outages. 

The hat will allow for a DC power source to be attached to it in the form of a battery with a voltage ranging from 7.5 - 12V, this voltage will then be stepped down to a stable 5V and will power the PI. The voltmeter will allow the PI to monitor the battery voltage. The Pi will then use the data to trigger a corresponding amount of LEDs, so that the user will have a visual indicator of the battery’s voltage.
